<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Genus;
use Doctrine\ORM\EntityRepository;


class GenusNoteRepository extends EntityRepository
{
    /**
     * @param Genus $genus
     * @return Genus[]
     */
    public function findAllRecentNotesForGenus(Genus $genus)
    {
        return $this->createQueryBuilder("genus_note")
            ->andWhere('genus_note.createdAt > :dateFrom')
            ->andWhere('genus_note.genus = :genus')
            ->setParameter('dateFrom', new \DateTime('-3 months'))
            ->setParameter('genus', $genus)
            ->orderBy('genus_note.createdAt', 'DESC')
            ->getQuery()
            ->execute();
    }
}