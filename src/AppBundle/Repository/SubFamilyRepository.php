<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class SubFamilyRepository extends EntityRepository
{
    public function createAlphabeticalQueryBuilder()
    {
        return $this->createQueryBuilder('sub_family')
            ->orderBy('sub_family.name', 'ASC');
    }

    public function findAllQueryBuilder($filter = '')
    {
        $qb = $this->createQueryBuilder('sub_family');

        if ($filter) {
            $qb->andWhere('sub_family.name LIKE :filter')
                ->setParameter('filter', '%'.$filter.'%');
        }

        return $qb;
    }
}