<?php

namespace AppBundle\Repository;


use AppBundle\Entity\Genus;
use AppBundle\Entity\SubFamily;
use Doctrine\ORM\EntityRepository;

class GenusRepository extends EntityRepository
{
    /**
     * @return Genus[]
     */
    public function findAllPublishedOrderedByRecentlyActive()
    {
        return $this->createQueryBuilder('genus')
            ->andWhere('genus.isPublished = :isPublished')
            ->setParameter('isPublished', true)
            ->leftJoin('genus.notes', 'genus_note')
            ->orderBy('genus_note.createdAt', 'DESC')
            ->getQuery()
            ->execute();
    }

    public function findAllQueryBuilder($filter = '')
    {
        $qb = $this->createQueryBuilder('genus');

        if ($filter) {
            $qb->andWhere('genus.name LIKE :filter OR genus.funFact LIKE :filter')
                ->setParameter('filter', '%'.$filter.'%');
        }

        return $qb;
    }

    public function findAllBySubfamilyQueryBuilder(SubFamily $subFamily)
    {
        $qb = $this->createQueryBuilder('genus');

        if ($subFamily) {
            $qb->andWhere('genus.subFamily = :subFamily')
                ->setParameter('subFamily', $subFamily);
        }

        return $qb;
    }
}