<?php

namespace AppBundle\Form;

use AppBundle\Entity\SubFamily;
use AppBundle\Repository\SubFamilyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GenusFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add(
                'subFamily',
                EntityType::class,
                [
                    'query_builder' => function (SubFamilyRepository $repository) {
                        return $repository->createAlphabeticalQueryBuilder();
                    },
                    'class' => SubFamily::class,
                    'placeholder' => 'Choose a SubFamily',
                ]
            )
            ->add('speciesCount')
            ->add(
                'funFact',
                TextType::class,
                [
                    'disabled' => $options['is_edit'],
                ]
            )
            ->add(
                'isPublished',
                ChoiceType::class,
                [
                    'choices' => [
                        'Yes' => true,
                        'No' => false,
                    ],
                ]
            )
            ->add(
                'firstDiscoveredAt',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'attr' => ['class' => 'js-datepicker'],
                    'html5' => false,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'AppBundle\Entity\Genus',
                'csrf_protection' => false,
                'is_edit' => false,
                'allow_extra_fields' => true,
            ]
        );

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_genus_form_type';
    }
}
