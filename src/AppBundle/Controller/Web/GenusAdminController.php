<?php

namespace AppBundle\Controller\Web;


use AppBundle\Entity\Genus;
use AppBundle\Form\GenusFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GenusAdminController
 * @package AppBundle\Controller
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class GenusAdminController extends Controller
{
    /**
     * @Route("/admin/genus")
     */
    public function adminAction()
    {

    }

    /**
     * @Route("/admin/genus/list", name="admin_genus_list")
     */
    public function listAction()
    {
        //$this->denyAccessUnlessGranted('ROLE_ADMIN');

        $em = $this->getDoctrine()->getManager();

        $genuses = $em->getRepository('AppBundle:Genus')->findAll();

        //dump($_SESSION);
        return $this->render('admin/genus/list.html.twig', ['genuses' => $genuses]);
    }

    /**
     * @Route("/genus/new", name="admin_genus_new")
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(GenusFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $genus = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash('success', 'Genus Created!');

            return $this->redirectToRoute('admin_genus_list');
        }

        return $this->render(
            'admin/genus/new.html.twig',
            [
                'genusForm' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/genus/{id}/edit", name="admin_genus_edit")
     *
     * @param Request $request
     * @param Genus $genus
     * @return Response
     */
    public function editAction(Request $request, Genus $genus)
    {
        $form = $this->createForm(GenusFormType::class, $genus);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $genus = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash('success', 'Genus Updated!');

            return $this->redirectToRoute('admin_genus_list');
        }

        return $this->render(
            'admin/genus/edit.html.twig',
            [
                'genusForm' => $form->createView(),
            ]
        );
    }

}