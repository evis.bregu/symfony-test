<?php

namespace AppBundle\Controller\Web;

use AppBundle\Entity\Genus;
use AppBundle\Entity\GenusNote;
use MongoDB\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GenusController
 * @package AppBundle\Controller
 */
class GenusController extends Controller
{
    /**
     * @Route("/genus/new_old")
     */
    public function newAction()
    {
        $genus = new Genus();
        $genus->setName("Octopus".rand(1, 100));
        $genus->setSubFamily('Octopodinae');
        $genus->setSpeciesCount(rand(100, 99999));

        $note = new GenusNote();
        $note->setUsername('AquaWeaver');
        $note->setUserAvatarFilename('ryan.jpeg');
        $note->setNote('I counted 8 legs... as they wrapped around me');
        $note->setCreatedAt(new \DateTime('-1 month'));
        $note->setGenus($genus);

        $em = $this->getDoctrine()->getManager();
        $em->persist($genus);
        $em->persist($note);
        $em->flush();

        return new Response('<html><body>Genus created!</body></html>');
    }

    /**
     * @Route("/genus")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        $genuses = $em->getRepository('AppBundle:Genus')->findAllPublishedOrderedByRecentlyActive();

        return $this->render('genus/list.html.twig', ['genuses' => $genuses]);
    }

    /**
     * @Route("/genus/{genusName}", name="genus_show")
     *
     * @param $genusName string
     * @return Response
     */
    public function showAction($genusName)
    {

//        $cache = $this->get('doctrine_cache.providers.my_markdown_cache');
//        $key = md5($funFact);
//
//        if ($cache->contains($key)) {
//            $funFact = $cache->fetch($key);
//        } else {
//            sleep(2);
//            $funFact = $this->container->get('markdown.parser')
//                ->transform($funFact);
//            $cache->save($key, $funFact);
//        }

        $em = $this->getDoctrine()->getManager();
        $genus = $em->getRepository('AppBundle:Genus')->findOneBy(['name' => $genusName]);

        if (!$genus) {
            throw $this->createNotFoundException('genus not found');
        }


        $funFact = $this->get("app.markdown_transformer")->parse($genus->getFunFact());

        $recentNotes = $em->getRepository("AppBundle:GenusNote")->findAllRecentNotesForGenus($genus);

        return $this->render(
            'genus/show.html.twig',
            array(
                'genus' => $genus,
                'recentNoteCount' => count($recentNotes),
                "funFact" => $funFact,
            )
        );
    }

    /**
     * @Route("/genus/{name}/notes", name="genus_show_notes")
     * @Method("GET")
     * @param Genus $genus
     * @return Response
     */
    public function getNotesAction(Genus $genus)
    {
        $notes = array();
        foreach ($genus->getNotes() as $note) {

            $notes[] = [
                'id' => $note->getId(),
                'username' => $note->getUsername(),
                'avatarUri' => '/images/'.$note->getUserAvatarFileName(),
                'note' => $note->getNote(),
                'date' => $note->getCreatedAt()->format('M d, Y'),
            ];
        }

        $funFact = 'Octopuses can change the color of their body in just *three-tenths* of a second!';

        $data = [
            'notes' => $notes,
            'funFact' => $funFact,
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/mongo")
     */
    public function showMongo()
    {
        // Configuration
        $dbhost = 'mongodb://localhost/';
        $dbname = 'test';

        //Set typeMap conversion options to array. This way ther esults from MongoDB will be converted to php arrays.
        $options = array(
            'typeMap' => array(
                'array' => 'array',
                'root' => 'array',
                'document' => 'array',
            ),
        );

        $collection = (new Client($dbhost, array(), $options))->$dbname->routing_rules;
        $result = $collection->findOne(["id" => 29]);

        return new Response(
            '<html><body><pre>'.print_r($result, true).'</pre></body></html>'
        );
    }
}