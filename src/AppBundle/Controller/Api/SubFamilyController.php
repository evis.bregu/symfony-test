<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\SubFamily;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SubFamilyController extends BaseController
{
    /**
     * @Route("/api/subfamilies/{id}", name = "api_show_subfamily")
     * @Method("GET")
     *
     * @param integer $id
     * @return Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $subFamily = $em->getRepository("AppBundle:SubFamily")->findOneBy(['id' => $id]);

        if (!$subFamily) {
            throw $this->createNotFoundException(
                (
                    'No subfamily found with id "'.$id.'"!!'
                )
            );
        }

        $response = $this->createApiResponse($subFamily);

        return $response;
    }

    /**
     * @Route("/api/subfamilies", name="api_list_subfamilies")
     * @Method("GET")
     *
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        $filter = $request->query->get('filter');

        $qb = $this->getDoctrine()
            ->getRepository('AppBundle:SubFamily')
            ->findAllQueryBuilder($filter);

        $paginatedCollection = $this->get('pagination_factory')
            ->createCollection($qb, $request, 'api_list_subfamilies');

        $response = $this->createApiResponse($paginatedCollection, 200);

        return $response;
    }

    /**
     * @Route("/api/subfamilies/{id}/genuses", name="api_list_genuses_by_subfamily")
     * @Method("GET")
     * @param SubFamily $subFamily
     * @param Request $request
     * @return Response
     * @internal param $id
     */
    public function listGenusesAction(SubFamily $subFamily, Request $request)
    {

        $qb = $this->getDoctrine()
            ->getRepository('AppBundle:Genus')
            ->findAllBySubfamilyQueryBuilder($subFamily);

        $paginatedCollection = $this->get('pagination_factory')
            ->createCollection($qb, $request, 'api_list_genuses_by_subfamily', ["id" => $subFamily]);

        $response = $this->createApiResponse($paginatedCollection, 200);

        return $response;
    }
}