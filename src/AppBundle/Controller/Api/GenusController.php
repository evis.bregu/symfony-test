<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Genus;
use AppBundle\Entity\SubFamily;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GenusController
 * @package AppBundle\Controller\Api
 * @Security("is_granted('ROLE_USER')")
 */
class GenusController extends BaseController
{

    /**
     * @Route("/api/genuses", name="api_create_genus")
     * @Method("POST")
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getManager();

        $genus = new Genus();
        $form = $this->createForm("AppBundle\Form\GenusFormType", $genus);
        $this->processForm($request, $form);
        if (!$form->isValid()) {
            return $this->throwApiProblemValidationException($form);
        }

        $subFamily = new SubFamily();
        $subFamily->setName($data["subFamily"]);

        $genus->setSubFamily($subFamily);

        $em->persist($genus);
        $em->persist($subFamily);
        $em->flush();

        $response = $this->createApiResponse($genus, 201);

        $genusUrl = $this->generateUrl(
            "api_show_genus",
            ['id' => $genus->getId()]
        );

        $response->headers->set('Location', $genusUrl);

        return $response;
    }

    /**
     * @Route("/api/genuses/{id}", name = "api_show_genus")
     * @Method("GET")
     *
     * @param integer $id
     * @return Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $genus = $em->getRepository("AppBundle:Genus")->findOneBy(['id' => $id]);

        if (!$genus) {
            throw $this->createNotFoundException(
                (
                    'No genus found with id "'.$id.'"!!'
                )
            );
        }

        $response = $this->createApiResponse($genus);

        return $response;
    }

    /**
     * @Route("/api/genuses", name="api_list_genuses")
     * @Method("GET")
     *
     * @param Request $request
     * @return Response
     */
    public function listAction(Request $request)
    {
        $filter = $request->query->get('filter');

        $qb = $this->getDoctrine()
            ->getRepository('AppBundle:Genus')
            ->findAllQueryBuilder($filter);

        $paginatedCollection = $this->get('pagination_factory')
            ->createCollection($qb, $request, 'api_list_genuses');

        $response = $this->createApiResponse($paginatedCollection, 200);

        return $response;
    }

    /**
     * @Route("/api/genuses/{id}", name="api_update_genus")
     * @Method({"PUT", "PATCH"})
     *
     * @param Request $request
     * @param integer $id
     *
     * @return Response
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $genus = $em->getRepository("AppBundle:Genus")->findOneBy(['id' => $id]);

        if (!$genus) {
            throw $this->createNotFoundException(
                sprintf(
                    'No genus found with id "%s"',
                    $id
                )
            );
        }

        $form = $this->createForm("AppBundle\Form\UpdateGenusFormType", $genus);
        $this->processForm($request, $form);

        if (!$form->isValid()) {
            $this->get("logger")->error($form->getErrors());

            return $this->throwApiProblemValidationException($form);
        }

        $response = $this->createApiResponse($genus);

        return $response;
    }

    /**
     * @Route("/api/genuses/{id}", name="api_delete_genus")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param integer $id
     *
     * @return Response
     */
    public function deleteAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $genus = $em->getRepository("AppBundle:Genus")->findOneBy(['id' => $id]);
        if ($genus) {
            $em->remove($genus);
            $em->flush();
        }

        $response = $this->createApiResponse("OK", 204);

        return $response;
    }
}
