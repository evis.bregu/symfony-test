<?php

namespace Tests\AppBundle\Controller\Api;

use AppBundle\Test\ApiTestCase;
use JMS\Serializer\SerializationContext;

class GenusControllerTest extends ApiTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->createUser('filanfisteku', 'I<3Pizza');
    }

    public function testPostGenus()
    {
        $name = "SuperAwsomeGenus";
        $subFamily = $this->createSubFamily("SubGenus".rand(1, 100));
        $speciesCount = rand(1, 1000);
        $funFact = "Lorem Ipsum Sit Amet";
        $isPublished = true;
        $firstDiscoveredAt = (new \DateTime('-1 month'));

        $data = array(
            "name" => $name,
            "subFamily" => $subFamily->getId(),
            "speciesCount" => $speciesCount,
            "funFact" => $funFact,
            "isPublished" => $isPublished,
            "firstDiscoveredAt" => $firstDiscoveredAt->format('Y-m-d H:i:s'),
        );

        $response = $this->client->post(
            '/api/genuses',
            [
                'body' => json_encode($data),
                'headers' => $this->getAuthorizedHeaders('filanfisteku'),
            ]
        );

        $this->assertEquals('application/json', $response->getHeader('Content-Type'));

        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue($response->hasHeader('Location'));
        $finishedData = json_decode($response->getBody(true), true);
        $this->assertArrayHasKey("name", $finishedData);
        $this->assertEquals("SuperAwsomeGenus", $finishedData["name"]);
    }

    public function testGETGenus()
    {
        $subFamily = "SubGenus".rand(1, 100);
        $createdGenus = $this->createGenus(
            array(
                "name" => "SuperAwsomeGenus",
                "subFamily" => $subFamily,
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        $response = $this->client->get('/api/genuses/'.$createdGenus->getId(), [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);
        //$data = $response->json();

        $this->assertEquals(200, $response->getStatusCode());

        $this->asserter()->assertResponsePropertiesExist(
            $response,
            array(
                "name",
                "subFamily",
                "speciesCount",
                "funFact",
                "firstDiscoveredAt",
                "isPublished",
            )
        );

        $this->asserter()->assertResponsePropertyEquals($response, "_embedded.subFamily.name", $subFamily);
        $this->asserter()->assertResponsePropertyEquals($response, 'name', 'SuperAwsomeGenus');
        $this->asserter()->assertResponsePropertyEquals($response, 'speciesCount', $createdGenus->getSpeciesCount());
        $this->asserter()->assertResponsePropertyEquals(
            $response,
            '_links.self',
            $this->adjustUri('/api/genuses/'.$createdGenus->getId())
        );
    }

    public function testGETGenusDeep()
    {
        $createdGenus = $this->createGenus(
            array(
                "name" => "SuperAwsomeGenus",
                "subFamily" => "SubGenus".rand(1, 100),
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        $response = $this->client->get('/api/genuses/'.$createdGenus->getId().'?deep=1',[
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);
        //$data = $response->json();

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertiesExist(
            $response,
            array(
                'subFamily.name',
            )
        );
    }

    public function testGETGenusesCollection()
    {
        $this->createGenus(
            array(
                "name" => "SuperAwsomeGenus",
                "subFamily" => "SubGenus".rand(1, 100),
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        $this->createGenus(
            array(
                "name" => "UltraAwsomeGenus",
                "subFamily" => "SubGenus".rand(1, 100),
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        $response = $this->client->get('/api/genuses', [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);
        //$data = $response->json();

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists($response, 'items');
        $this->asserter()->assertResponsePropertyIsArray($response, 'items');

        $this->asserter()->assertResponsePropertyEquals($response, 'items[0].name', 'SuperAwsomeGenus');
        $this->asserter()->assertResponsePropertyEquals($response, 'items[1].name', 'UltraAwsomeGenus');

    }

    public function testGETGenusesCollectionPaginated()
    {
        $this->createGenus(
            array(
                "name" => "WILLNOTMATCH",
                "subFamily" => "SubGenus".rand(1, 100),
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        for ($i = 0; $i < 25; $i++) {
            $this->createGenus(
                array(
                    "name" => "AwsomeGenus".$i,
                    "subFamily" => "SubGenus".rand(1, 100),
                    "speciesCount" => rand(1, 1000),
                    "funFact" => "Lorem Ipsum Sit Amet".$i,
                    "isPublished" => true,
                    "firstDiscoveredAt" => (new \DateTime('-'.$i.' month')),
                )
            );
        }

        $response = $this->client->get('/api/genuses?filter=awsome', [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists($response, 'items');
        $this->asserter()->assertResponsePropertyIsArray($response, 'items');
        $this->asserter()->assertResponsePropertyEquals(
            $response,
            'items[5].name',
            'AwsomeGenus5'
        );
        $this->asserter()->assertResponsePropertyEquals($response, 'count', 10);
        $this->asserter()->assertResponsePropertyEquals($response, 'total', 25);
        $this->asserter()->assertResponsePropertyExists($response, '_links.next');

        // page 2
        $nextLink = $this->asserter()->readResponseProperty($response, '_links.next');
        $response = $this->client->get($nextLink, [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyEquals(
            $response,
            'items[5].name',
            'AwsomeGenus15'
        );
        $this->asserter()->assertResponsePropertyEquals($response, 'count', 10);

        $lastLink = $this->asserter()->readResponseProperty($response, '_links.last');
        $response = $this->client->get($lastLink, [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyEquals(
            $response,
            'items[4].name',
            'AwsomeGenus24'
        );
        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'items[5].name');
        $this->asserter()->assertResponsePropertyEquals($response, 'count', 5);
    }

    public function testPUTGenus()
    {
        $createdGenus = $this->createGenus(
            array(
                "name" => "SuperAwsomeGenusToPUT",
                "subFamily" => "SubGenus".rand(1, 100),
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        $createdGenus->setName("SuperAwsomeGenusUpdated");

        $context = new SerializationContext();
        $context->setSerializeNull(true);

        //Serialize only Default group, otherwise form validation will fail for subFamily
        $groups = array('Default');
        $context->setGroups($groups);

        $data = $this->getService('jms_serializer')->serialize($createdGenus, 'json', $context);
        $response = $this->client->put(
            '/api/genuses/'.$createdGenus->getId(),
            [
                'body' => $data,
                'headers' => $this->getAuthorizedHeaders('filanfisteku'),
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyEquals($response, 'name', 'SuperAwsomeGenusUpdated');
        $this->asserter()->assertResponsePropertyEquals($response, 'funFact', 'Lorem Ipsum Sit Amet');
    }

    public function testPATCHGenus()
    {
        $createdGenus = $this->createGenus(
            array(
                "name" => "SuperAwsomeGenusToPATCH",
                "subFamily" => "SubGenus".rand(1, 100),
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        $data = array(
            "name" => "SuperAwsomeGenusPatched",
        );

        $response = $this->client->patch(
            '/api/genuses/'.$createdGenus->getId(),
            [
                'body' => json_encode(($data)),
                'headers' => $this->getAuthorizedHeaders('filanfisteku'),
            ]
        );

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyEquals($response, 'name', 'SuperAwsomeGenusPatched');
        $this->asserter()->assertResponsePropertyEquals($response, 'funFact', 'Lorem Ipsum Sit Amet');
    }

    public function testDELETEGenus()
    {
        $createdGenus = $this->createGenus(
            array(
                "name" => "SuperAwsomeGenusToPUT",
                "subFamily" => "SubGenus".rand(1, 100),
                "speciesCount" => rand(1, 1000),
                "funFact" => "Lorem Ipsum Sit Amet",
                "isPublished" => true,
                "firstDiscoveredAt" => (new \DateTime('-1 month')),
            )
        );

        $response = $this->client->delete('/api/genuses/'.$createdGenus->getId(), [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testValidations()
    {

        $subFamily = "SubGenus".rand(1, 100);
        $speciesCount = rand(1, 1000);
        $funFact = "Lorem Ipsum Sit Amet";
        $isPublished = true;
        $firstDiscoveredAt = (new \DateTime('-1 month'));

        $data = array(
            "subFamily" => $subFamily,
            "speciesCount" => $speciesCount,
            "funFact" => $funFact,
            "isPublished" => $isPublished,
            "firstDiscoveredAt" => $firstDiscoveredAt->format('Y-m-d H:i:s'),
        );

        $response = $this->client->post(
            '/api/genuses',
            [
                'body' => json_encode($data),
                'headers' => $this->getAuthorizedHeaders('filanfisteku'),
            ]
        );

        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type'));
        $this->assertEquals(400, $response->getStatusCode());
        $this->asserter()->assertResponsePropertiesExist(
            $response,
            array(
                'type',
                'title',
                'errors',
            )
        );
        $this->asserter()->assertResponsePropertyExists($response, 'errors.name');
        $this->asserter()->assertResponsePropertyEquals($response, 'errors.name[0]', 'Please enter a valid name');
        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'errors.speciesCount');
        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'errors.funFact');
        $this->asserter()->assertResponsePropertyDoesNotExist($response, 'errors.isPublished');
    }

    public function testInvalidJson()
    {
        $invalidBody = <<<EOF
{
    "name" : "test",
    "subFamily" => "test"
    "speciesCount" => 34,
    "funFact" => "Lorem Ipsum",
    "isPublished" => 1,
    "firstDiscoveredAt" => "2017-04-08 10:29:44"
}
EOF;
        $response = $this->client->post(
            '/api/genuses',
            [
                'body' => $invalidBody,
                'headers' => $this->getAuthorizedHeaders('filanfisteku'),
            ]
        );

        $this->assertEquals(400, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyContains($response, 'type', 'invalid_body_format');
    }

    public function test404Exception()
    {
        $response = $this->client->get('/api/genuses/97108', [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('application/problem+json', $response->getHeader('Content-Type'));
        $this->asserter()->assertResponsePropertyEquals($response, 'type', 'about:blank');
        $this->asserter()->assertResponsePropertyEquals($response, 'title', 'Not Found');
        $this->asserter()->assertResponsePropertyEquals($response, 'detail', 'No genus found with id "97108"!!');
    }

    public function testRequiresAuthentication()
    {
        $subFamily = $this->createSubFamily("SubGenus".rand(1, 100));
        $data = array(
            "name" => "SuperAwsomeGenus",
            "subFamily" => $subFamily->getId(),
            "speciesCount" => rand(1, 1000),
            "funFact" => "Lorem Ipsum Sit Amet",
            "isPublished" => true,
            "firstDiscoveredAt" => "2017-04-08 10:29:44",
        );
        $response = $this->client->post(
            '/api/genuses',
            [
                'body' => json_encode($data),
            ]
        );

        $this->assertEquals(401, $response->getStatusCode());
    }
}
