<?php
/**
 * Created by PhpStorm.
 * User: evis
 * Date: 5/9/17
 * Time: 3:20 PM
 */

namespace Tests\AppBundle\Controller\Api;


use AppBundle\Test\ApiTestCase;

class SubFamilyControllerTest extends ApiTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->createUser('filanfisteku', 'I<3Pizza');
    }

    public function testGETGenusesBySubFamily(){

        $subFamily = $this->createSubFamily("AwsomeSubFamily");

        for ($i = 0; $i < 25; $i++) {
            $this->createGenusSingleSubFamily(
                array(
                    "name" => "AwsomeGenus".$i,
                    "subFamily" => "SubGenus".rand(1, 100),
                    "speciesCount" => rand(1, 1000),
                    "funFact" => "Lorem Ipsum Sit Amet".$i,
                    "isPublished" => true,
                    "firstDiscoveredAt" => (new \DateTime('-'.$i.' month')),
                ), $subFamily
            );
        }

        //First get the subfamily from its endpoint
        $response = $this->client->get('/api/subfamilies/'.$subFamily->getId(), [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);
        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists($response, "_links.genuses");

        $genusesLink = $this->asserter()->readResponseProperty($response, "_links.genuses");
        $response = $this->client->get($genusesLink, [
            'headers' => $this->getAuthorizedHeaders('filanfisteku'),
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists($response, 'items');
    }
}