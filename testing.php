<?php

use GuzzleHttp\Client;

require __DIR__."/vendor/autoload.php";

$client = new Client([
    'base_url' => 'http://localhost:8000',
    'defaults' => [
        'exceptions' => false
    ]
]);



$name = "Genus".rand(1, 100);
$subFamily = "SubGenus".rand(1, 100);
$speciesCount = rand(1, 1000);
$funFact = "Lorem Ipsum Sit Amet";
$isPublished = true;
$firstDiscoveredAt = (new \DateTime('-1 month'));

$data = array(
    "name" => $name,
    "subFamily" => $subFamily,
    "speciesCount" => $speciesCount,
    "funFact" => $funFact,
    "isPublished" => $isPublished,
    "firstDiscoveredAt" => $firstDiscoveredAt->format('Y-m-d H:i:s')
);
$response = $client->post('/api/genuses', [
    'body' => json_encode($data)
]);

echo $response;
echo "\n\n";

$showGenusUrl = $response->getHeader('Location');

$response = $client->get($showGenusUrl);

echo $response;
echo "\n\n";